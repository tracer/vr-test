// Wait for the A-Frame scene to load
document.querySelector('a-scene').addEventListener('loaded', function () {
  // Get a reference to the camera entity
  var camera = document.querySelector('#camera')

  // Set the initial position of the camera
  var radius = 20 // Radius of the circular path
  var angle = 0 // Initial angle

  // Function to update the camera position in a circular path
  function updateCameraPosition() {
    // Calculate the new camera position in the circular path
    var x = radius * Math.cos(angle)
    var z = radius * Math.sin(angle)

    // Update the camera's position

    // camera.setAttribute('position', `0 0 20`)
    camera.setAttribute('position', `${x} 1 ${z}`)
    angle += 0.001 // Adjust the speed of rotation as needed

    // Request the next animation frame
    requestAnimationFrame(updateCameraPosition)
  }

  // Start the animation loop
  updateCameraPosition()
})
